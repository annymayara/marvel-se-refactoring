public class Exemplo {

    /**
     * Método que calcula preço total aplicando o desconto
     * @param quantidade quantidade de produtos
     * @param valorUnitario valor do produto
     * @return valor calculado
     */
    public Double calculaPreco(int quantidade, int valorUnitario) {
        return calculaValorTotal(quantidade, valorUnitario) * calculaFatorDesconto(quantidade, valorUnitario);
    }

    private double calculaFatorDesconto(int quantidade, int valorUnitario) {
        if (produtoCaro(calculaValorTotal(quantidade, valorUnitario)))
            return 0.95;
        else
            return  0.98;
    }

    private int calculaValorTotal(int quantidade, int valorUnitario) {
        return quantidade * valorUnitario;
    }

    private boolean produtoCaro(int valorTotal) {
        return valorTotal > 1000;
    }
}
