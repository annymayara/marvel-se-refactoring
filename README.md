# MARVEL-SE Assemble Workshop

## O Guia Prático do Bom Desenvolvedor: Mantendo o Código Limpo Organizado

Código usado de exemplo para mostrar uma refatoração.

## Refatorações Usadas:

- Rename Variable
- Change Function Declaration
- Extract Function
- Replace Command with Function
- Decompose Conditional
- Replace Nested Conditional with Guard Clauses

Fonte: https://refactoring.com/catalog
